<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agendamento extends Model
{
    protected $table = 'agendamento';

    public $timestamps = false;

    protected $guarded = [
        'id',
    ];

    protected $dates = [
        'data',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function($row) {
           $row->uid = str_random(40);
        });
    }

    public function Doador()
    {
        return $this->belongsTo(\App\Doador::class, 'doador_id', 'id');
    }

    public function Laboratorio()
    {
        return $this->belongsTo(\App\Laboratorio::class, 'laboratorio_id', 'id');
    }
}
