<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doador extends Model
{
    protected $table = 'doador';

    protected $guarded = [
        'id',
    ];

    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($row) {
           $row->uid = str_random(40);
        });
    }

    public function Usuario()
    {
        return $this->belongsTo(\App\Usuario::class, 'id', 'usuario_id');
    }

    public function Agendamentos()
    {
        return $this->hasMany(\App\Agendamento::class, 'doador_id', 'id');
    }
}
