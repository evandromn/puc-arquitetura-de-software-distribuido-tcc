<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use Validator;
use GuzzleHttp\Client as Guzzle;

use App\Http\Controllers\Controller;
use App\Agendamento;
use App\Token;

class AgendamentoController extends Controller
{
    public function index()
    {
        return Agendamento::query()
            ->where('laboratorio_id', Auth::user()->laboratorio->id)
            ->orderBy('id', 'desc')
            ->get();
    }

    public function show($id)
    {
        $agendamento = Agendamento::query()
            ->where('id', $id)
            ->where('laboratorio_id', Auth::user()->laboratorio->id)
            ->orderBy('id', 'desc')
            ->get();

        if ($agendamento->count() == 0) {
            return abort(404);
        }

        return $agendamento->first();
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'doador_id' => 'required|min:1|exists:doador,id',
            'data'      => 'required|date',
        ]);

        if ($validator->fails()) {
            return [
                'errors' => $validator->errors(),
            ];
        }

        return Agendamento::create([
            'data'              => Carbon::parse($request->data),
            'doador_id'         => $request->doador_id,
            'laboratorio_id'    => Auth::user()->laboratorio->id
        ]);
    }

    public function delete($id)
    {
        $agendamento = Agendamento::findOrFail($id);

        if ($agendamento->laboratorio->id == Auth::user()->laboratorio->id) {
            $agendamento->delete();

            return [
                'status' => 'Agendamento excluído com sucesso.',
            ];
        }

        return [
            'error' => 'Não é possível deletar agendamento de outro laboratório.',
        ];
    }

    public function solicitacao()
    {
        $guzzle = new Guzzle;

        $solicitacoesRequest = $guzzle->request('GET', env('TOKEN_SOLICITACAO_BASE_URL') . '/api/compartilhados/solicitacoes', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . Token::getSolicitacaoToken(),
            ],
        ]);

        return $solicitacoesRequest;
    }

    public function atendimento(Request $request)
    {
        $guzzle = new Guzzle;

        $atendimentoRequest = $guzzle->request('POST', env('TOKEN_ATENDIMENTO_BASE_URL') . '/api/compartilhados/atendimentos', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . Token::getAtendimentoToken(),
            ],
            'form_params' => [
                'solicitacao_uid' => $request->solicitacao_uid,
                'laboratorio_uid' => Auth::user()->laboratorio->uid,
            ]
        ]);

        return $atendimentoRequest;
    }
}
