<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use Validator;

use App\Http\Controllers\Controller;
use App\Doador;

class DoadorController extends Controller
{
    public function index()
    {
        return Doador::query()
            ->orderBy('id', 'desc')
            ->get();
    }

    public function show($id)
    {
        return Doador::findOrFail($id);
    }
}
