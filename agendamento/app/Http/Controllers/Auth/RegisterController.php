<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Carbon\Carbon;

use App\Usuario;
use App\Doador;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/doadores/painel';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nome' => 'required|string|max:255',
            'sobrenome' => 'required|string|max:255',
            'cpf' => 'required|string|max:255',
            'telefone' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:usuario',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $usuario = Usuario::create([
            'tipo'      => 'doador',
            'email'     => $data['email'],
            'password'  => bcrypt($data['password']),
        ]);

        $data['saude_doou_anteriormente'] = array_key_exists('saude_doou_anteriormente', $data) ? 1 : 0;
        $data['saude_foi_operado'] = array_key_exists('saude_foi_operado', $data) ? 1 : 0;
        $data['saude_teve_convulsoes'] = array_key_exists('saude_teve_convulsoes', $data) ? 1 : 0;
        $data['saude_tomou_vacina_recentemente'] = array_key_exists('saude_tomou_vacina_recentemente', $data) ? 1 : 0;

        $usuario->doador()->create([
            'nome' => $data['nome'],
            'sobrenome' => $data['sobrenome'],
            'telefone' => $data['telefone'],
            'cpf' => $data['cpf'],
            'data_nascimento' => Carbon::parse($data['data_nascimento']),
            'saude_doou_anteriormente' => $data['saude_doou_anteriormente'],
            'saude_foi_operado' => $data['saude_foi_operado'],
            'saude_teve_convulsoes' => $data['saude_teve_convulsoes'],
            'saude_tomou_vacina_recentemente' => $data['saude_tomou_vacina_recentemente'],
        ]);

        return $usuario;
    }
}
