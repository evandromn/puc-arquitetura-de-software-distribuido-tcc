<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use Validator;

use App\Agendamento;
use App\Laboratorio;

class DoadorAgendamentoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $laboratorios = Laboratorio::all();

        return view('doador.agendamento.create', [
            'laboratorios' => $laboratorios,
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->only([
            'laboratorio_id',
            'data',
        ]);

        $validator = Validator::make($request->all(), [
            'laboratorio_id'    => 'required|min:1',
            'data'              => 'required|date',
        ]);

        if ($validator->fails()) {
            return back()
                ->withInput($data)
                ->withErrors($validator->errors());
        }

        Agendamento::create([
            'data'              => Carbon::parse($data['data']),
            'doador_id'         => Auth::user()->doador->id,
            'laboratorio_id'    => $data['laboratorio_id'],
        ]);

        return redirect()->route('doador.dashboard')->with(['message' => 'O agendamento foi realizado com sucesso!']);
    }

    public function delete($id)
    {
        $agendamento = Agendamento::findOrFail($id);

        $agendamento->delete();

        return redirect()->route('doador.dashboard')->with(['message' => 'O agendamento foi cancelado com sucesso!']);
    }
}
