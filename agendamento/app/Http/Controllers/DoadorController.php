<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Agendamento;

class DoadorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        $agendamentos = Agendamento::query()
            ->where('doador_id', Auth::user()->doador->id)
            ->orderBy('id', 'desc')
            ->get();

        return view('doador.dashboard', [
            'agendamentos' => $agendamentos,
        ]);
    }
}
