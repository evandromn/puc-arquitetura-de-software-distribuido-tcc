<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use Validator;

use App\Agendamento;
use App\Doador;

class LaboratorioAgendamentoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $doadores = Doador::all();

        return view('laboratorio.agendamento.create', [
            'doadores' => $doadores,
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->only([
            'doador_id',
            'data',
        ]);

        $validator = Validator::make($request->all(), [
            'doador_id'    => 'required|min:1',
            'data'         => 'required|date',
        ]);

        if ($validator->fails()) {
            return back()
                ->withInput($data)
                ->withErrors($validator->errors());
        }

        Agendamento::create([
            'data'              => Carbon::parse($data['data']),
            'doador_id'         => $data['doador_id'],
            'laboratorio_id'    => Auth::user()->laboratorio->id,
        ]);

        return redirect()->route('laboratorio.dashboard')->with(['message' => 'O agendamento foi realizado com sucesso!']);
    }

    public function delete($id)
    {
        $agendamento = Agendamento::findOrFail($id);

        if ($agendamento->laboratorio->id == Auth::user()->laboratorio->id) {
            $agendamento->delete();

            return redirect()->route('laboratorio.dashboard')->with(['message' => 'O agendamento foi cancelado com sucesso!']);
        } else {
            return redirect()->route('laboratorio.dashboard')->with(['error' => 'Não é possível deletar agendamento de outro laboratório!']);
        }

    }
}
