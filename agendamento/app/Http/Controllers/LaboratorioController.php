<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client as Guzzle;
use Auth;

use App\Agendamento;
use App\Token;

class LaboratorioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        $guzzle = new Guzzle;

        $agendamentos = Agendamento::query()
            ->where('laboratorio_id', Auth::user()->laboratorio->id)
            ->orderBy('id', 'desc')
            ->get();

        $solicitacoesRequest = $guzzle->request('GET', env('TOKEN_SOLICITACAO_BASE_URL') . '/api/compartilhados/solicitacoes', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . Token::getSolicitacaoToken(),
            ],
        ]);

        return view('laboratorio.dashboard', [
            'agendamentos' => $agendamentos,
            'solicitacoes' => json_decode($solicitacoesRequest->getBody()),
        ]);
    }

    public function atendimento(Request $request)
    {
        $guzzle = new Guzzle;

        $atendimentoRequest = $guzzle->request('POST', env('TOKEN_ATENDIMENTO_BASE_URL') . '/api/compartilhados/atendimentos', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . Token::getAtendimentoToken(),
            ],
            'form_params' => [
                'solicitacao_uid' => $request->solicitacao_uid,
                'laboratorio_uid' => Auth::user()->laboratorio->uid,
            ]
        ]);

        $atendimento = json_decode($atendimentoRequest->getBody());

        return redirect()->route('laboratorio.dashboard')
            ->with([
                'message' => 'O atendimento da solicitação foi registrado com sucesso!'
                    . ' O código do atendimento é: ' . $atendimento->uid
            ]);
    }
}
