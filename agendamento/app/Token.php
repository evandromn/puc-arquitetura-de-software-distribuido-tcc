<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;

class Token extends Model
{
    protected static $tokenFilenameSolicitacao = 'tokens/solicitacao';

    public static function setTokenFilenameSolicitacao($filename)
    {
        self::$tokenFilenameSolicitacao = $filename;
    }

    public static function getTokenFilenameSolicitacao()
    {
        return self::$tokenFilenameSolicitacao;
    }

    public static function getSolicitacaoToken(callable $callbackGetToken = null)
    {
        $tokenFilename = self::getTokenFilenameSolicitacao();

        if (Storage::exists($tokenFilename)) {
            $token = Storage::get($tokenFilename);
        } else {
            if ($callbackGetToken !== null) {
                $token = $callbackGetToken();
            } else {
                $token = self::getRemoteToken([
                    'token_url' => env('TOKEN_SOLICITACAO_URL'),
                    'token_client_id' => env('TOKEN_SOLICITACAO_CLIENT_ID'),
                    'token_secret' => env('TOKEN_SOLICITACAO_SECRET'),
                ]);
            }

            Storage::put($tokenFilename, $token);
        }

        return $token;
    }

    public static function getAtendimentoToken()
    {
        $tokenFilename = 'tokens/atendimento';

        if (Storage::exists($tokenFilename)) {
            $token = Storage::get($tokenFilename);
        } else {
            $guzzle = new Guzzle;

            $response = $guzzle->post(env('TOKEN_ATENDIMENTO_URL'), [
                'form_params' => [
                    'grant_type' => 'client_credentials',
                    'client_id' => env('TOKEN_ATENDIMENTO_CLIENT_ID'),
                    'client_secret' => env('TOKEN_ATENDIMENTO_SECRET'),
                ],
            ]);

            $token = json_decode((string) $response->getBody(), true)['access_token'];

            Storage::put($tokenFilename, $token);
        }

        return $token;
    }

    public static function getRemoteToken($params)
    {
        $guzzle = new \GuzzleHttp\Client;

        $response = $guzzle->post($params['token_url'], [
            'form_params' => [
                'grant_type' => 'client_credentials',
                'client_id' => $params['token_client_id'],
                'client_secret' => $params['token_secret'],
            ],
        ]);

        return json_decode((string) $response->getBody(), true)['access_token'];
    }
}
