<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoadorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doador', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uid');
            $table->string('cpf');
            $table->string('nome');
            $table->string('sobrenome');
            $table->string('telefone');
            $table->timestamp('data_nascimento');
            $table->boolean('saude_doou_anteriormente')->nullable();
            $table->boolean('saude_foi_operado')->nullable();
            $table->boolean('saude_teve_convulsoes')->nullable();
            $table->boolean('saude_tomou_vacina_recentemente')->nullable();
            $table->integer('usuario_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doador');
    }
}
