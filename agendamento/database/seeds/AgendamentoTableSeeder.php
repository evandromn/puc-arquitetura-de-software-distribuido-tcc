<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

use App\Agendamento;

class AgendamentoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
         Agendamento::create([
             'id'               => 1,
             'doador_id'        => 1,
             'laboratorio_id'   => 2,
             'data'             => Carbon::now()->subDays(600),
         ]);

         Agendamento::create([
             'id'               => 2,
             'doador_id'        => 3,
             'laboratorio_id'   => 3,
             'data'             => Carbon::now()->subDays(350),
         ]);

         Agendamento::create([
             'id'               => 3,
             'doador_id'        => 2,
             'laboratorio_id'   => 1,
             'data'             => Carbon::now()->subDays(300),
         ]);

         Agendamento::create([
             'id'               => 4,
             'doador_id'        => 1,
             'laboratorio_id'   => 3,
             'data'             => Carbon::now()->addDays(180),
         ]);

         Agendamento::create([
             'id'               => 5,
             'doador_id'        => 2,
             'laboratorio_id'   => 1,
             'data'             => Carbon::now()->addDays(190),
         ]);

         Agendamento::create([
             'id'               => 6,
             'doador_id'        => 3,
             'laboratorio_id'   => 2,
             'data'             => Carbon::now()->addDays(200),
         ]);
     }
}
