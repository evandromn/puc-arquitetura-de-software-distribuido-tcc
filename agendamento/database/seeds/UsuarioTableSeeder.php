<?php

use Illuminate\Database\Seeder;

use App\Usuario;

class UsuarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Usuario::create([
            'id'        => 1,
            'email'     => 'joao.silva@exemplo.com.br',
            'password'  => bcrypt('123456'),
            'tipo'      => 'doador',
        ])->doador()->create([
            'id'            => 1,
            'cpf'           => '999.999.999-99',
            'nome'          => 'João',
            'sobrenome'     => 'Silva',
            'telefone'      => '(31) 99999-9999',
        ]);
        Usuario::create([
            'id'        => 2,
            'email'     => 'laboratorio.santa.luzia@exemplo.com.br',
            'password'  => bcrypt('123456'),
            'tipo'      => 'laboratorio',
        ])->laboratorio()->create([
            'id'            => 1,
            'nome'          => 'Laboratório Santa Luzia',
        ]);
        Usuario::create([
            'id'        => 3,
            'email'     => 'jose.castro@exemplo.com.br',
            'password'  => bcrypt('123456'),
            'tipo'      => 'doador',
        ])->doador()->create([
            'id'            => 2,
            'cpf'           => '999.999.999-99',
            'nome'          => 'José',
            'sobrenome'     => 'Castro',
            'telefone'      => '(31) 99999-9999',
        ]);
        Usuario::create([
            'id'        => 4,
            'email'     => 'laboratorio.luxemburgo@exemplo.com.br',
            'password'  => bcrypt('123456'),
            'tipo'      => 'laboratorio',
        ])->laboratorio()->create([
            'id'           => 2,
            'nome'         => 'Laboratório Luxemburgo',
        ]);
        Usuario::create([
            'id'        => 5,
            'email'     => 'miguel.martins@exemplo.com.br',
            'password'  => bcrypt('123456'),
            'tipo'      => 'doador',
        ])->doador()->create([
            'id'            => 3,
            'cpf'           => '999.999.999-99',
            'nome'          => 'Miguel',
            'sobrenome'     => 'Martins',
            'telefone'      => '(31) 99999-9999',
        ]);
        Usuario::create([
            'id'        => 6,
            'email'     => 'laboratorio.aroeira@exemplo.com.br',
            'password'  => bcrypt('123456'),
            'tipo'      => 'laboratorio',
        ])->laboratorio()->create([
            'id'           => 3,
            'nome'         => 'Laboratório Aroeira',
        ]);
    }
}
