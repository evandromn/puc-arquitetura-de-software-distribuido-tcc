@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if ( session()->has('message') )
                <div class="alert alert-success alert-dismissable">{{ session()->get('message') }}</div>
            @endif
            @if ( session()->has('error') )
                <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
            @endif

            <h1>Painel do Doador: {{ Auth::user()->doador->nome }} {{ Auth::user()->doador->sobrenome }}</h1>

            <div class="panel panel-default">
                <div class="panel-heading">
                    Agendamentos
                    <a href="{{ route('doador.agendamento.create') }}" class="btn btn-xs btn-primary pull-right">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Novo agendamento
                    </a>
                </div>

                <div class="panel-body">
                    @if (count($agendamentos) > 0)
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Nome do Laboratório</th>
                                    <th>Data</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($agendamentos as $agendamento)
                                    <tr class="{{ $agendamento->data > \Carbon\Carbon::now() ? 'success' : 'warning' }}">
                                        <td>{{ $agendamento->laboratorio->nome }}</td>
                                        <td>{{ $agendamento->data->format('d/m/Y à\s H:i:s') }}</td>
                                        <td>
                                            @if ($agendamento->data > \Carbon\Carbon::now())
                                            <form method="POST" action="{{ route('doador.agendamento.delete', ['id' => $agendamento->id])}}">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-xs btn-default">
                                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                    Cancelar
                                                </button>
                                            </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        Nenhum agendamento encontrado.
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
