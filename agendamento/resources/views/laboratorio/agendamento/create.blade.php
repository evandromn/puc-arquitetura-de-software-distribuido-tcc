@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Agendamento de Doação</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('laboratorio.agendamento.store') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('doador_id') ? ' has-error' : '' }}">
                            <label for="doador_id" class="col-md-4 control-label">Laboratório</label>

                            <div class="col-md-6">
                                <select name="doador_id" id="doador_id" class="form-control">
                                    <option value="" {{ old('doador_id') == false ? 'selected' : '' }}>Selecione</option>
                                    @foreach ($doadores as $doador)
                                        <option value="{{ $doador->id }}" {{ old('doador_id') == $doador->id ? 'selected' : '' }}>{{ $doador->nome }} {{ $doador->sobrenome }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('doador_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('doador_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('data') ? ' has-error' : '' }}">
                            <label for="data" class="col-md-4 control-label">Data</label>

                            <div class="col-md-6">
                                <input id="data" type="datetime-local" class="form-control" name="data" value="{{ old('data') }}" required autofocus>

                                @if ($errors->has('data'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('data') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Agendar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
