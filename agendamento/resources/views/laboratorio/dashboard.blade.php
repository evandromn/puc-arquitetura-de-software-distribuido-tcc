@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if ( session()->has('message') )
                <div class="alert alert-success alert-dismissable">{{ session()->get('message') }}</div>
            @endif
            @if ( session()->has('error') )
                <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
            @endif

            <h1>Painel do Laboratório: {{ Auth::user()->laboratorio->nome }}</h1>

            <div class="panel panel-default">
                <div class="panel-heading">
                    Agendamentos
                    <a href="{{ route('laboratorio.agendamento.create') }}" class="btn btn-xs btn-primary pull-right">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Novo agendamento
                    </a>
                </div>

                <div class="panel-body">
                    @if (count($agendamentos) > 0)
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Nome do Doador</th>
                                    <th>Data</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($agendamentos as $agendamento)
                                    <tr class="{{ $agendamento->data > \Carbon\Carbon::now() ? 'success' : 'warning' }}">
                                        <td>{{ $agendamento->doador->nome }} {{ $agendamento->doador->sobrenome }}</td>
                                        <td>{{ $agendamento->data->format('d/m/Y à\s H:i:s') }}</td>
                                        <td>
                                            @if ($agendamento->data > \Carbon\Carbon::now())
                                            <form method="POST" action="{{ route('laboratorio.agendamento.delete', ['id' => $agendamento->id])}}">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-xs btn-default">
                                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                    Cancelar
                                                </button>
                                            </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        Nenhum agendamento encontrado.
                    @endif
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    Solicitações Abertas de Bolsa de Sangue
                </div>

                <div class="panel-body">
                    @if (count($solicitacoes) > 0)
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Paciente</th>
                                    <th>Grupo Sanguíneo</th>
                                    <th>Fator RH</th>
                                    <th>Modalidade</th>
                                    <th>Diagnóstico</th>
                                    <th>Hospital</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($solicitacoes as $solicitacao)
                                    <tr>
                                        <td>{{ $solicitacao->paciente->nome }} {{ $solicitacao->paciente->sobrenome }}</td>
                                        <td>{{ $solicitacao->paciente->grupo_sanguineo }}</td>
                                        <td>{{ $solicitacao->paciente->fator_rh }}</td>
                                        <td>{{ $solicitacao->modalidade }}</td>
                                        <td>{{ $solicitacao->diagnostico }}</td>
                                        <td>{{ $solicitacao->paciente->hospital->nome }}</td>
                                        <td>
                                            <form method="POST" action="{{ route('laboratorio.atendimento') }}">
                                                {{ csrf_field() }}
                                                {{ method_field('POST') }}
                                                <input type="hidden" name="solicitacao_uid" value="{{ $solicitacao->uid }}">
                                                <button type="submit" class="btn btn-xs btn-default">
                                                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                                    Atender
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        Nenhuma solicitação em aberto encontrada.
                    @endif
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    OAuth
                </div>

                <div class="panel-body">
                    <passport-clients></passport-clients>
                    <passport-authorized-clients></passport-authorized-clients>
                    <passport-personal-access-tokens></passport-personal-access-tokens>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
