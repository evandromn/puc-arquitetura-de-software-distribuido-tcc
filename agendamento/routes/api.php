<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/agendamentos', 'Api\AgendamentoController@index')->middleware('auth:api');
Route::get('/agendamentos/{id}', 'Api\AgendamentoController@show')->middleware('auth:api');
Route::post('/agendamentos', 'Api\AgendamentoController@create')->middleware('auth:api');
Route::delete('/agendamentos/{id}', 'Api\AgendamentoController@delete')->middleware('auth:api');

Route::get('/doadores', 'Api\DoadorController@index')->middleware('auth:api');
Route::get('/doadores/{id}', 'Api\DoadorController@show')->middleware('auth:api');

Route::get('/solicitacoes', 'Api\AgendamentoController@solicitacao')->middleware('auth:api');
Route::post('/atendimentos', 'Api\AgendamentoController@atendimento')->middleware('auth:api');
