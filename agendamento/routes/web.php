<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
        if (Auth::user()->tipo == 'doador') {
            return redirect()->route('doador.dashboard');
        } else {
            return redirect()->route('laboratorio.dashboard');
        }
    } else {
        return view('welcome');
    }
});

Auth::routes();

Route::get('/laboratorios/painel', 'LaboratorioController@dashboard')
    ->name('laboratorio.dashboard')
    ->middleware('can:gerenciar-laboratorio');
    
Route::post('/laboratorios/atendimentos', 'LaboratorioController@atendimento')
    ->name('laboratorio.atendimento')
    ->middleware('can:gerenciar-laboratorio');

Route::get('/laboratorios/agendamentos/criar', 'LaboratorioAgendamentoController@create')
    ->name('laboratorio.agendamento.create')
    ->middleware('can:gerenciar-laboratorio');

Route::post('/laboratorios/agendamentos/criar', 'LaboratorioAgendamentoController@store')
    ->name('laboratorio.agendamento.store')
    ->middleware('can:gerenciar-laboratorio');

Route::delete('/laboratorios/agendamentos/{id}', 'LaboratorioAgendamentoController@delete')
    ->name('laboratorio.agendamento.delete')
    ->middleware('can:gerenciar-laboratorio');


Route::get('/doadores/painel', 'DoadorController@dashboard')
    ->name('doador.dashboard')
    ->middleware('can:gerenciar-doador');

Route::get('/doadores/agendamentos/criar', 'DoadorAgendamentoController@create')
    ->name('doador.agendamento.create')
    ->middleware('can:gerenciar-doador');

Route::post('/doadores/agendamentos/criar', 'DoadorAgendamentoController@store')
    ->name('doador.agendamento.store')
    ->middleware('can:gerenciar-doador');

Route::delete('/doadores/agendamentos/{id}', 'DoadorAgendamentoController@delete')
    ->name('doador.agendamento.delete')
    ->middleware('can:gerenciar-doador');
