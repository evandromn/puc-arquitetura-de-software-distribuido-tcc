<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TokenTest extends TestCase
{
    public function testTokenSolicitacao()
    {
        $fakeTokenFilename = 'tests/tokens/solicitacao';
        $fakeToken = 'abcdefghijklmnopqrstuvwxyz';

        \App\Token::setTokenFilenameSolicitacao($fakeTokenFilename);

        \Storage::delete($fakeTokenFilename);

        $token = \App\Token::getSolicitacaoToken(function () use ($fakeToken) {
            return $fakeToken;
        });

        $tokenFileContent = \Storage::get($fakeTokenFilename);

        $this->assertFileExists(storage_path('app/' . $fakeTokenFilename));
        $this->assertStringEqualsFile(storage_path('app/' . $fakeTokenFilename), $token);
        $this->assertEquals($fakeToken, $tokenFileContent);
    }
}
