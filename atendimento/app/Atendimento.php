<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atendimento extends Model
{
    protected $table = 'atendimento';

    protected $dates = ['data'];

    protected $guarded = ['atendimento_uid'];

    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($row) {
            $row->uid = str_random(40);
        });
    }
}
