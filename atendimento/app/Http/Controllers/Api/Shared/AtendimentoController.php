<?php

namespace App\Http\Controllers\Api\Shared;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
use GuzzleHttp\Client as Guzzle;

use App\Http\Controllers\Controller;
use App\Atendimento;
use App\Token;

class AtendimentoController extends Controller
{
    public function create(Request $request)
    {
        $data = $request->only([
            'solicitacao_uid',
            'laboratorio_uid',
        ]);

        $validator = Validator::make($request->all(), [
            'solicitacao_uid'   => 'required',
            'laboratorio_uid'   => 'required',
        ]);

        if ($validator->fails()) {
            return [
                'errors' => $validator->errors(),
            ];
        }

        $atendimento = Atendimento::create([
            'solicitacao_uid'   => $data['solicitacao_uid'],
            'laboratorio_uid'   => $data['laboratorio_uid'],
            'data'              => Carbon::now(),
        ]);

        $guzzle = new Guzzle;

        $atendimentoRequest = $guzzle->request('POST', env('TOKEN_SOLICITACAO_BASE_URL') . '/api/compartilhados/solicitacoes/atendimentos', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . Token::getSolicitacaoToken(),
            ],
            'form_params' => [
                'solicitacao_uid'   => $request->solicitacao_uid,
                'atendimento_uid'   => $atendimento->uid,
            ]
        ]);

        return $atendimento;
    }
}
