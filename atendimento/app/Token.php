<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client as Guzzle;
use Storage;

class Token extends Model
{
    public static function getSolicitacaoToken()
    {
        $tokenFilename = 'tokens/solicitacao';

        if (Storage::exists($tokenFilename)) {
            $token = Storage::get($tokenFilename);
        } else {
            $guzzle = new Guzzle;

            $response = $guzzle->post(env('TOKEN_SOLICITACAO_URL'), [
                'form_params' => [
                    'grant_type' => 'client_credentials',
                    'client_id' => env('TOKEN_SOLICITACAO_CLIENT_ID'),
                    'client_secret' => env('TOKEN_SOLICITACAO_SECRET'),
                ],
            ]);

            $token = json_decode((string) $response->getBody(), true)['access_token'];

            Storage::put($tokenFilename, $token);
        }

        return $token;
    }
}
