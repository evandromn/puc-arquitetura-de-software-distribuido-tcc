<?php

use Illuminate\Http\Request;

Route::post('/compartilhados/atendimentos', 'Api\Shared\AtendimentoController@create')->middleware('client_credentials');
