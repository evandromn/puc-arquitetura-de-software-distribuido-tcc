<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hospital extends Model
{
    protected $table = 'hospital';

    protected $guarded = ['id'];

    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($row) {
           $row->uid = str_random(40);
        });
    }

    public function Usuario()
    {
        return $this->belongsTo(\App\Usuario::class, 'id', 'usuario_id');
    }

    public function Solicitacoes()
    {
        return $this->hasMany(\App\Solicitacao::class, 'hospital_id', 'id');
    }
}
