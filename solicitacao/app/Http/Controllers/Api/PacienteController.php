<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;
use Validator;

use App\Paciente;

class PacienteController extends Controller
{
    public function index()
    {
        return Paciente::query()
            ->where('hospital_id', Auth::user()->hospital->id)
            ->orderBy('id', 'desc')
            ->get();
    }

    public function create(Request $request)
    {
        $data = $request->only([
            'nome',
            'sobrenome',
        ]);

        $validator = Validator::make($request->all(), [
            'nome'      => 'required',
            'sobrenome' => 'required',
        ]);

        if ($validator->fails()) {
            return [
                'errors' => $validator->errors(),
            ];
        }

        return Paciente::create([
            'nome'          => $data['nome'],
            'sobrenome'     => $data['sobrenome'],
            'data'          => Carbon::now(),
            'hospital_id'   => Auth::user()->hospital->id,
        ]);
    }

    public function show($id)
    {
        $paciente = Paciente::query()
            ->where('hospital_id', Auth::user()->hospital->id)
            ->where('id', $id)
            ->get()->first();

        if ($paciente == null) {
            return abort(404);
        }

        return $paciente;
    }

    public function delete($id)
    {
        $paciente = Paciente::query()
            ->where('hospital_id', Auth::user()->hospital->id)
            ->where('id', $id)
            ->get()->first();

        if ($paciente == null) {
            return abort(404);
        }

        if (count($paciente->solicitacoes) == 0) {
            $paciente->delete();

            return [
                'status' => 'Paciente excluído com sucesso.',
            ];
        }

        return [
            'error' => 'Paciente com solicitações não pode ser excluído',
        ];
    }
}
