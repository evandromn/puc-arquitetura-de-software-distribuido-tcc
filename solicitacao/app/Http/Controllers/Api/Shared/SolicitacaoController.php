<?php

namespace App\Http\Controllers\Api\Shared;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;

use App\Http\Controllers\Controller;
use App\Solicitacao;

class SolicitacaoController extends Controller
{
    public function index()
    {
        return Solicitacao::query()
            ->whereNull('solicitacao.atendimento_uid')
            ->orderBy('solicitacao.id', 'asc')
            ->with('paciente.hospital')
            ->get();
    }

    public function atendimento(Request $request)
    {
        $solicitacao = Solicitacao::query()
            ->where('uid', $request->solicitacao_uid)
            ->get()->first();

        $solicitacao->atendimento_uid = $request->atendimento_uid;
        $solicitacao->save();

        return $solicitacao;
    }
}
