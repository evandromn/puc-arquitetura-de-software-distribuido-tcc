<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use Validator;

use App\Http\Controllers\Controller;
use App\Solicitacao;

class SolicitacaoController extends Controller
{
    public function index()
    {
        return Solicitacao::query()
            ->addSelect('solicitacao.*')
            ->join('paciente', 'paciente.id', 'solicitacao.paciente_id')
            ->where('paciente.hospital_id', Auth::user()->hospital->id)
            ->orderBy('solicitacao.id', 'desc')
            ->get();
    }

    public function show($id)
    {
        $solicitacao = Solicitacao::query()
            ->addSelect('solicitacao.*')
            ->join('paciente', 'paciente.id', 'solicitacao.paciente_id')
            ->where('paciente.hospital_id', Auth::user()->hospital->id)
            ->where('solicitacao.id', $id)
            ->get()
            ->first();

        if ($solicitacao == null) {
            return abort(404);
        }

        return $solicitacao;
    }

    public function create(Request $request)
    {
        $data = $request->only([
            'modalidade',
            'diagnostico',
            'indicacao_transfusional',
            'paciente_id',
        ]);

        $validator = Validator::make($request->all(), [
            'modalidade'      => 'required',
            'diagnostico' => 'required',
            'indicacao_transfusional' => 'required',
            'paciente_id' => 'required|min:1',
        ]);

        if ($validator->fails()) {
            return [
                'errors' => $validator->errors()
            ];
        }

        return Solicitacao::create([
            'modalidade'    => $data['modalidade'],
            'diagnostico'   => $data['diagnostico'],
            'indicacao_transfusional'   => $data['indicacao_transfusional'],
            'data'          => Carbon::now(),
            'paciente_id'   => $data['paciente_id'],
        ]);
    }

    public function delete($id)
    {
        $solicitacao = Solicitacao::query()
            ->addSelect('solicitacao.*')
            ->join('paciente', 'paciente.id', 'solicitacao.paciente_id')
            ->where('paciente.hospital_id', Auth::user()->hospital->id)
            ->where('solicitacao.id', $id)
            ->get()->first();

        if ($solicitacao == null) {
            return abort(404);
        }

        $solicitacao->delete();

        return [
            'status' => 'Solicitação excluída com sucesso.',
        ];
    }
}
