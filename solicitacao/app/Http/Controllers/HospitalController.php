<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Solicitacao;
use App\Paciente;

class HospitalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        $solicitacoes = Solicitacao::query()
            ->addSelect('solicitacao.*')
            ->join('paciente', 'paciente.id', 'solicitacao.paciente_id')
            ->where('paciente.hospital_id', Auth::user()->hospital->id)
            ->orderBy('solicitacao.id', 'desc')
            ->get();

        $pacientes = Paciente::query()
            ->where('hospital_id', Auth::user()->hospital->id)
            ->orderBy('id', 'desc')
            ->get();

        return view('hospital.dashboard', [
            'solicitacoes' => $solicitacoes,
            'pacientes' => $pacientes,
        ]);
    }
}
