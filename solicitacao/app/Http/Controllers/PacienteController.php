<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
use Auth;

use App\Paciente;

class PacienteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        return view('paciente.create', []);
    }

    public function store(Request $request)
    {
        $data = $request->only([
            'nome',
            'sobrenome',
            'grupo_sanguineo',
            'fator_rh',
        ]);

        $validator = Validator::make($request->all(), [
            'nome'              => 'required',
            'sobrenome'         => 'required',
            'grupo_sanguineo'   => 'required',
            'fator_rh'          => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withInput($data)
                ->withErrors($validator->errors());
        }

        Paciente::create([
            'nome'              => $data['nome'],
            'sobrenome'         => $data['sobrenome'],
            'grupo_sanguineo'   => $data['grupo_sanguineo'],
            'fator_rh'          => $data['fator_rh'],
            'data'              => Carbon::now(),
            'hospital_id'       => Auth::user()->hospital->id,
        ]);

        return redirect()->route('hospital.dashboard')->with(['message' => 'O cadastro do paciente foi realizado com sucesso!']);
    }

    public function delete($id)
    {
        $paciente = Paciente::findOrFail($id);

        if (count($paciente->solicitacoes) == 0) {
            $paciente->delete();
            return redirect()->route('hospital.dashboard')->with(['message' => 'O paciente foi excluído com sucesso!']);
        } else {
            return redirect()->route('hospital.dashboard')->with(['error' => 'Não é possível excluir um paciente com solicitações de bolsas de sangue!']);
        }
    }
}
