<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use Carbon\Carbon;

use App\Solicitacao;
use App\Paciente;

class SolicitacaoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $pacientes = Paciente::where('hospital_id', Auth::user()->hospital->id)
            ->get();

        return view('solicitacao.create', [
            'pacientes' => $pacientes,
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->only([
            'modalidade',
            'diagnostico',
            'indicacao_transfusional',
            'paciente_id',
        ]);

        $validator = Validator::make($request->all(), [
            'modalidade'                => 'required',
            'diagnostico'               => 'required',
            'indicacao_transfusional'   => 'required',
            'paciente_id'               => 'required|min:1',
        ]);

        if ($validator->fails()) {
            return back()
                ->withInput($data)
                ->withErrors($validator->errors());
        }

        Solicitacao::create([
            'modalidade'                => $data['modalidade'],
            'diagnostico'               => $data['diagnostico'],
            'indicacao_transfusional'   => $data['indicacao_transfusional'],
            'data'                      => Carbon::now(),
            'paciente_id'               => $data['paciente_id'],
        ]);

        return redirect()->route('hospital.dashboard')->with(['message' => 'A solicitação foi realizada com sucesso!']);
    }

    public function delete($id)
    {
        $solicitacao = Solicitacao::findOrFail($id);

        $solicitacao->delete();

        return redirect()->route('hospital.dashboard')->with(['message' => 'A solicitação foi excluída com sucesso!']);
    }
}
