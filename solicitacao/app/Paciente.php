<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    protected $table = 'paciente';

    protected $guarded = ['id'];

    public $timestamps = false;

    protected $dates = ['data'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function($row) {
           $row->uid = str_random(40);
        });
    }

    public function Solicitacoes()
    {
        return $this->hasMany(\App\Solicitacao::class, 'paciente_id', 'id');
    }

    public function Hospital()
    {
        return $this->belongsTo(\App\Hospital::class, 'hospital_id', 'id');
    }
}
