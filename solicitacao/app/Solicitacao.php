<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitacao extends Model
{
    protected $table = 'solicitacao';

    protected $guarded = ['id'];

    public $timestamps = false;

    protected $dates = ['data'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function($row) {
           $row->uid = str_random(40);
        });
    }

    public function Paciente()
    {
        return $this->belongsTo(\App\Paciente::class, 'paciente_id', 'id');
    }
}
