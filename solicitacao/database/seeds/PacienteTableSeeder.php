<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

use App\Paciente;

class PacienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Paciente::create([
            'id'                => 1,
            'nome'              => 'Antônio',
            'sobrenome'         => 'Cruz',
            'grupo_sanguineo'   => 'A',
            'fator_rh'          => 'Negativo',
            'data'              => Carbon::now(),
            'hospital_id'       => 1,
        ]);

        Paciente::create([
            'id'                => 2,
            'nome'              => 'Luiz',
            'sobrenome'         => 'Xavier',
            'grupo_sanguineo'   => 'AB',
            'fator_rh'          => 'Positivo',
            'data'              => Carbon::now(),
            'hospital_id'       => 2,
        ]);

        Paciente::create([
            'id'                => 3,
            'nome'              => 'Maria',
            'sobrenome'         => 'de Jesus',
            'grupo_sanguineo'   => 'O',
            'fator_rh'          => 'Positivo',
            'data'              => Carbon::now(),
            'hospital_id'       => 1,
        ]);

        Paciente::create([
            'id'                => 4,
            'nome'              => 'Carla',
            'sobrenome'         => 'Viana',
            'grupo_sanguineo'   => 'B',
            'fator_rh'          => 'Negativo',
            'data'              => Carbon::now(),
            'hospital_id'       => 2,
        ]);
    }
}
