<?php

use Illuminate\Database\Seeder;

use App\Solicitacao;
use App\Hospital;
use Carbon\Carbon;

class SolicitacaoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Solicitacao::create([
            'id'                        => 1,
            'modalidade'                => 'Programada',
            'diagnostico'               => 'Anemia',
            'indicacao_transfusional'   => 'Transfusão de concentrado de hemácias',
            'data'                      => Carbon::now(),
            'paciente_id'               => 2,
        ]);

        Solicitacao::create([
            'id'                        => 2,
            'modalidade'                => 'Rotina',
            'diagnostico'               => 'Anemia aguda',
            'indicacao_transfusional'   => 'Transfusão de concentrado de hemácias',
            'data'                      => Carbon::now(),
            'paciente_id'               => 1,
        ]);

        Solicitacao::create([
            'id'                        => 3,
            'modalidade'                => 'Emergência',
            'diagnostico'               => 'Perda aguda sanguínea',
            'indicacao_transfusional'   => 'Transfusão de concentrado de hemácias',
            'data'                      => Carbon::now(),
            'paciente_id'               => 3,
        ]);

        Solicitacao::create([
            'id'                        => 4,
            'modalidade'                => 'Urgência',
            'diagnostico'               => 'Cirurgia cardíaca',
            'indicacao_transfusional'   => 'Tranfusão de plaquetas',
            'data'                      => Carbon::now(),
            'paciente_id'               => 4,
        ]);
    }
}
