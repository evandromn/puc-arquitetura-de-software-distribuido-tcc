<?php

use Illuminate\Database\Seeder;

use App\Usuario;
use App\Hospital;

class UsuarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Usuario::create([
            'id'        => 1,
            'email'     => 'sao.joao.de.deus@exemplo.com.br',
            'password'  => bcrypt('123456'),
        ])->hospital()->create([
            'id'        => 1,
            'nome'      => 'São João de Deus',
        ]);

        Usuario::create([
            'id'        => 2,
            'email'     => 'santa.casa@exemplo.com.br',
            'password'  => bcrypt('123456'),
        ])->hospital()->create([
            'id'        => 2,
            'nome'      => 'Santa Casa',
        ]);
    }
}
