@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if ( session()->has('message') )
                <div class="alert alert-success alert-dismissable">{{ session()->get('message') }}</div>
            @endif
            @if ( session()->has('error') )
                <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
            @endif

            <h1>Painel do Hospital: {{ Auth::user()->hospital->nome }}</h1>

            <div class="panel panel-default">
                <div class="panel-heading">
                    Solicitações de Bolsa de Sangue
                    <a href="{{ route('solicitacao.create') }}" class="btn btn-xs btn-primary pull-right margin-right-10">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nova solicitação
                    </a>
                </div>

                <div class="panel-body">
                    @if (count($solicitacoes) > 0)
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Paciente</th>
                                    <th>Data</th>
                                    <th>Modalidade</th>
                                    <th>Diagnóstico</th>
                                    <th>Indicação Transfusional</th>
                                    <th>Identificação do Atendimento</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($solicitacoes as $solicitacao)
                                    <tr class="{{ $solicitacao->atendimento_uid ? 'success' : 'warning' }}">
                                        <td>{{ $solicitacao->paciente->nome }} {{ $solicitacao->paciente->sobrenome }}</td>
                                        <td>{{ $solicitacao->data->format('d/m/Y à\s H:i:s') }}</td>
                                        <td>{{ $solicitacao->modalidade }}</td>
                                        <td>{{ $solicitacao->diagnostico }}</td>
                                        <td>{{ $solicitacao->indicacao_transfusional }}</td>
                                        <td>
                                            @if ($solicitacao->atendimento_uid)
                                                <div class="input-group">
                                                  <div class="input-group-addon"><span class="glyphicon glyphicon-barcode" aria-hidden="true"></span></div>
                                                  <input type="text" class="form-control" value="{{ $solicitacao->atendimento_uid }}" readonly>
                                                </div>
                                            @else
                                                Ainda não atendido.
                                            @endif
                                        </td>
                                        <td>
                                            @if ($solicitacao->atendimento_uid == null)
                                                <form method="POST" action="{{ route('solicitacao.delete', ['id' => $solicitacao->id])}}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <button type="submit" class="btn btn-xs btn-default">
                                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                        Excluir
                                                    </button>
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        Nenhuma solicitação encontrada.
                    @endif
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    Pacientes
                    <a href="{{ route('paciente.create') }}" class="btn btn-xs btn-primary pull-right">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Novo paciente
                    </a>
                </div>

                <div class="panel-body">
                    @if (count($pacientes) > 0)
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Data</th>
                                    <th>Grupo Sanguíneo</th>
                                    <th>Fator RH</th>
                                    <th>Número de Solicitações</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pacientes as $paciente)
                                    <tr class="">
                                        <td>{{ $paciente->nome }} {{ $paciente->sobrenome }}</td>
                                        <td>{{ $paciente->data->format('d/m/Y à\s H:i:s') }}</td>
                                        <td>{{ $paciente->grupo_sanguineo }}</td>
                                        <td>{{ $paciente->fator_rh }}</td>
                                        <td>{{ $paciente->solicitacoes->count() }}</td>
                                        <td>
                                            @if ($paciente->solicitacoes->count() == 0)
                                            <form method="POST" action="{{ route('paciente.delete', ['id' => $paciente->id])}}">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-xs btn-default">
                                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                    Excluir
                                                </button>
                                            </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        Nenhum paciente encontrado.
                    @endif
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    OAuth
                </div>

                <div class="panel-body">
                    <passport-clients></passport-clients>
                    <passport-authorized-clients></passport-authorized-clients>
                    <passport-personal-access-tokens></passport-personal-access-tokens>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
