@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cadastro de Paciente</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('paciente.store') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
                            <label for="nome" class="col-md-4 control-label">Nome</label>

                            <div class="col-md-6">
                                <input id="nome" type="text" class="form-control" name="nome" value="{{ old('nome') }}" required autofocus>

                                @if ($errors->has('nome'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nome') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('sobrenome') ? ' has-error' : '' }}">
                            <label for="sobrenome" class="col-md-4 control-label">Sobrenome</label>

                            <div class="col-md-6">
                                <input id="sobrenome" type="text" class="form-control" name="sobrenome" value="{{ old('sobrenome') }}" required autofocus>

                                @if ($errors->has('sobrenome'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sobrenome') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('grupo_sanguineo') ? ' has-error' : '' }}">
                            <label for="grupo_sanguineo" class="col-md-4 control-label">Grupo Sanguíneo</label>

                            <div class="col-md-6">
                                <select name="grupo_sanguineo" class="form-control">
                                    <option value="" {{ old('grupo_sanguineo') == '' ? 'selected' : '' }}>Selecione</option>
                                    <option value="A" {{ old('grupo_sanguineo') == 'A' ? 'selected' : '' }}>A</option>
                                    <option value="B" {{ old('grupo_sanguineo') == 'B' ? 'selected' : '' }}>B</option>
                                    <option value="AB" {{ old('grupo_sanguineo') == 'AB' ? 'selected' : '' }}>AB</option>
                                    <option value="O" {{ old('grupo_sanguineo') == 'O' ? 'selected' : '' }}>O</option>
                                </select>

                                @if ($errors->has('grupo_sanguineo'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('grupo_sanguineo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('fator_rh') ? ' has-error' : '' }}">
                            <label for="fator_rh" class="col-md-4 control-label">Fator RH</label>

                            <div class="col-md-6">
                                <div class="radio-inline">
                                  <label>
                                    <input type="radio" name="fator_rh" value="Positivo" checked>
                                    Positivo
                                  </label>
                                </div>
                                <div class="radio-inline">
                                  <label>
                                    <input type="radio" name="fator_rh"  value="Negativo">
                                    Negativo
                                  </label>
                                </div>

                                @if ($errors->has('fator_rh'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fator_rh') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Cadastrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
