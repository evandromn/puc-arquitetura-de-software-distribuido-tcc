@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cadastro de Solicitação de Bolsa de Sangue</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('solicitacao.store') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('paciente_id') ? ' has-error' : '' }}">
                            <label for="paciente_id" class="col-md-4 control-label">Paciente</label>

                            <div class="col-md-6">
                                <select name="paciente_id" class="form-control">
                                    <option value="" {{ old('paciente_id') == false ? 'selected' : '' }}>Selecione</option>
                                    @foreach ($pacientes as $paciente)
                                        <option value="{{ $paciente->id }}" {{ old('paciente_id') == $paciente->id ? 'selected' : '' }}>{{ $paciente->nome }} {{ $paciente->sobrenome }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('paciente_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('paciente_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('modalidade') ? ' has-error' : '' }}">
                            <label for="modalidade" class="col-md-4 control-label">Modalidade</label>

                            <div class="col-md-6">
                                <select name="modalidade" class="form-control">
                                    <option value="" {{ old('modalidade') == '' ? 'selected' : '' }}>Selecione</option>
                                    <option value="Programada" {{ old('modalidade') == 'Programada' ? 'selected' : '' }}>Programada</option>
                                    <option value="Rotina" {{ old('modalidade') == 'Rotina' ? 'selected' : '' }}>Rotina (em até 24h)</option>
                                    <option value="Urgência" {{ old('modalidade') == 'Urgência' ? 'selected' : '' }}>Urgência (em até 3h)</option>
                                    <option value="Emergência" {{ old('modalidade') == 'Emergência' ? 'selected' : '' }}>Emergência (retardo da transfusão acarreta risco à vida)</option>
                                </select>

                                @if ($errors->has('modalidade'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('modalidade') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('diagnostico') ? ' has-error' : '' }}">
                            <label for="diagnostico" class="col-md-4 control-label">Diagnóstico</label>

                            <div class="col-md-6">
                                <input id="diagnostico" type="text" class="form-control" name="diagnostico" value="{{ old('diagnostico') }}" required autofocus>

                                @if ($errors->has('diagnostico'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('diagnostico') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('indicacao_transfusional') ? ' has-error' : '' }}">
                            <label for="indicacao_transfusional" class="col-md-4 control-label">Indicação Transfusional</label>

                            <div class="col-md-6">
                                <input id="indicacao_transfusional" type="text" class="form-control" name="indicacao_transfusional" value="{{ old('indicacao_transfusional') }}" required autofocus>

                                @if ($errors->has('indicacao_transfusional'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('indicacao_transfusional') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Cadastrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
