<?php

use Illuminate\Http\Request;

//
// API com operações específica para cada hospital
//
Route::get('/pacientes', 'Api\PacienteController@index')->middleware('auth:api');
Route::get('/pacientes/{id}', 'Api\PacienteController@show')->middleware('auth:api');
Route::post('/pacientes', 'Api\PacienteController@create')->middleware('auth:api');
Route::delete('/pacientes/{id}', 'Api\PacienteController@delete')->middleware('auth:api');

Route::get('/solicitacoes', 'Api\SolicitacaoController@index')->middleware('auth:api');
Route::get('/solicitacoes/{id}', 'Api\SolicitacaoController@show')->middleware('auth:api');
Route::post('/solicitacoes', 'Api\SolicitacaoController@create')->middleware('auth:api');
Route::delete('/solicitacoes/{id}', 'Api\SolicitacaoController@delete')->middleware('auth:api');


//
// API com operações compartilhadas e que não fazem referência ao usuário específico
//
Route::get('/compartilhados/solicitacoes', 'Api\Shared\SolicitacaoController@index')->middleware('client_credentials');
Route::post('/compartilhados/solicitacoes/atendimentos', 'Api\Shared\SolicitacaoController@atendimento')->middleware('client_credentials');
