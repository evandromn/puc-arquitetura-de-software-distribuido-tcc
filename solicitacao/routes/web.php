<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
        return redirect()->route('hospital.dashboard');
    } else {
        return view('welcome');
    }
});

Auth::routes();

Route::get('/hospitais/painel', 'HospitalController@dashboard')->name('hospital.dashboard');

Route::get('/pacientes/criar', 'PacienteController@create')->name('paciente.create');
Route::post('/pacientes/criar', 'PacienteController@store')->name('paciente.store');
Route::delete('/pacientes/{id}', 'PacienteController@delete')->name('paciente.delete');

Route::get('/solicitacoes/criar', 'SolicitacaoController@create')->name('solicitacao.create');
Route::post('/solicitacoes/criar', 'SolicitacaoController@store')->name('solicitacao.store');
Route::delete('/solicitacoes/{id}', 'SolicitacaoController@delete')->name('solicitacao.delete');
